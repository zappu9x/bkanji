package com.example.vetrio.converter;

import android.util.Log;

/**
 * Created by nguye on 9/22/2015.
 */
public class ConvertString {
    public String getMean(String content) {
        String string = "";
        char a[] = content.toCharArray();
        boolean isStart = false;
        boolean isContinue = false;
        for (int i = 0; i < content.length(); i++) {
            if (a[i] == '◆') {
                isStart = true;
            }
            if (a[i] == '※' || a[i] == '-' || a[i] == '∴' || a[i] == '☆') {
                isStart = false;
            }
            if (isStart == true && a[i] != '◆') {
                string += a[i];
                isContinue = true;
            }
            if (isContinue == true && a[i] == '◆') {
                string += '/';
                isContinue = true;
            }
        }
        return string;
    }


    public String getMeanKanji(String content) {
        String string = "";
        char a[] = content.toCharArray();
        boolean isStart = false;
        boolean isContinue = false;
        for (int i = 0; i < content.length(); i++) {
            if (a[i] == '：') {
                isStart = true;
            }
            if (a[i] == ';') {
                isStart = false;
            }
            if (isStart == true && a[i] != '：') {
                string += a[i];
                isContinue = true;
            }
            if (isContinue == true && a[i] == ';') {
                string += '/';
                isContinue = false;
            }
        }
        return string;
    }

    public String getHtml(String word, String content) {
        String string = "";
        string += "<H3><Font color = \"#dc0e00\">" + word + "</Font></H3><H4><ul>";
        char a[] = content.toCharArray();
        String openCard = "";
        String closeCard = "";
        for (int i = 0; i < content.length(); i++) {
            if (a[i] == '∴') {
                string += closeCard;
                openCard = "</ul><br/><Font color = \"#dc0e00\">";
                closeCard = "</Font>";
                string += openCard;
            }
            if (a[i] == '☆') {
                string += closeCard;
                openCard = "</ul><br/><Font color = \"#8706B0\">";
                closeCard = "</Font><ul>";
                string += openCard;
            }
            if (a[i] == '◆') {
                string += closeCard;
                openCard = "</ul><li><Font color = \"#00ca00\">";
                closeCard = "</Font><ul>";
                string += openCard;
            }

            if (a[i] == '※') {
                string += closeCard;
                openCard = "<li> <a target=\"_blank\" href=\"entry://example\">";
                closeCard = "</a>";
                string += openCard;
            }

            if (a[i] == ':') {
                string += closeCard;
                closeCard = "</li>";
            }
            if (a[i] != '※' && a[i] != '◆' && a[i] != '∴')
                string += a[i];
        }
        string += "</ul></H4>";
        Log.d("html", string);
        return string;
    }

    //String kanji, String hanviet, String radical, String strocke, String onread, String kunread, String example
    public String getHtmlKanji(String[] content) {
        String string = "";
        string += "<H2><Font color = \"#dc0e00\">" + content[0] + "</Font></H2><H4>"
                + "<Font color = \"#8706B0\">☆ Hán việt: <Big>" + content[1] + "</Big></Font>"
                + "<ul><li><Font color = \"#00ca00\">Radical: " + content[2] + "</Font></li>"
                + "<li><Font color = \"#00ca00\">Số nét: " + content[3] + "</Font></li></ul>"
                + "<Font color = \"#8706B0\">※ Onyomi: " + content[4] + "</Font><br/>"
                + "<Font color = \"#8706B0\">※ Kunyomi: </Font>"
                + "<ul><Font color = \"#4f5bc2\"><li>";
        char a[] = content[5].toCharArray();
        String openCard = "<li>";
        String closeCard = "</Font></li><Font color = \"#4f5bc2\">";
        for (int i = 0; i < content[5].length(); i++) {
            if (a[i] == '：') {
                string += "</Font><Font color = \"#000000\">";
            }
            if (a[i] == ';') {
                string += closeCard + openCard;
            }
            if (a[i] != ';') {
                string += a[i];
            }
        }

        // Example to html
        string += "</li></ul>"
                + "<Font color = \"#8706B0\">※ Ví dụ: </Font><ul>";

        if(content[6] != null) {
            a = content[6].toCharArray();
            openCard = "<li><Font color = \"#4f5bc2\">";
            closeCard = "</Font></li>";
            int numberOfDot = 0;
            for (int i = 0; i < content[6].length(); i++) {
                if (a[i] == ':') {
                    numberOfDot++;
                    string += "</Font><Font color = \"#4f5bc2\">";
                    if (numberOfDot >= 2) {
                        if (numberOfDot % 2 == 0) string += "<br/";
                        string += "</Font><Font color = \"#000000\">";
                        if (numberOfDot % 2 == 0) continue;
                    }

                }
                if (a[i] == '1' || a[i] == '2' || a[i] == '3' || a[i] == '4' || a[i] == '5') {
                    numberOfDot = 0;
                    string += closeCard + openCard;
                }
                string += a[i];
            }
        }

        string += "</li></ul></H4>";

        Log.d("html", string);
        Log.d("html122345",a.length + "");
        return string;
    }


}
