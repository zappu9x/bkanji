package com.example.vetrio.fingeritem;

/**
 * Created by nguye on 9/27/2015.
 */
public class FingerSearchItem {
    private String kanji;
    private float score;
    private String[] data;

    public FingerSearchItem(String kanji, float score) {
        this.kanji = kanji;
        this.score = score;
    }

    public float getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public String getKanji() {
        return kanji;
    }

    public void setKanji(String kanji) {
        this.kanji = kanji;
    }

    public String[] getData() {
        return data;
    }

    public void setData(String[] data) {
        this.data = data;
    }
}
