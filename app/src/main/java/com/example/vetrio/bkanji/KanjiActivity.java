package com.example.vetrio.bkanji;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.SQLException;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.vetrio.adapter.KanjiListViewAdapter;
import com.example.vetrio.converter.ConvertString;
import com.example.vetrio.converter.WanaKanaJava;
import com.example.vetrio.database.DataBaseHeplper;

public class KanjiActivity extends AppCompatActivity {

    private ListView list;
    private Toolbar toolbar;
    public SearchView searchView;
    private boolean canBack = false;

    private static final String DB_NAME = "bkanji.db";
    private static final String TABLE_NAME = "kanjidata";
    private static final String COLUMN_NAME = "hanviet";
    public DataBaseHeplper dataBaseHeplper;
    public Cursor cursor;
    private int queryCode = 0;
    private ProgressBar progressBar;
    WanaKanaJava wanaKana;
    private Context context = this;
    private KanjiListViewAdapter adapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kanji);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        list = (ListView) findViewById(R.id.list);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        progressBar.setVisibility(View.VISIBLE);

        wanaKana = new WanaKanaJava(true);

        //CreateDatabase
        dataBaseHeplper = new DataBaseHeplper(this, DB_NAME);
        try {
            dataBaseHeplper.openDataBase();
        } catch (SQLException sqle) {
            throw sqle;
        }

        AsyncTask<Void, Integer, Void> asyncTask = new AsyncTask<Void, Integer, Void>() {
            @Override
            protected void onPreExecute(){
            }
            @Override
            protected Void doInBackground(Void... voids) {
                canBack = false;
                cursor = dataBaseHeplper.getInformation(TABLE_NAME, COLUMN_NAME, "");
                cursor.moveToFirst();
                return null;
            }
            protected void onProgressUpdate(Integer... values){
            }
            protected void onPostExecute(Void result) {
                progressBar.setVisibility(View.GONE);
                canBack = true;
                adapter = new KanjiListViewAdapter(context, cursor);
                list.setAdapter(adapter);
            }
        };
        asyncTask.execute();

        toolbar.setTitle("Tra từ kanji");
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                String[] data = new String[7];
                for (int i = 0; i < 7; i++){
                    data[i] = cursor.getString(i+1);
                }
                Intent intent = new Intent(KanjiActivity.this, ResultSearchActivity.class);
                intent.putExtra("dataHtml", new ConvertString().getHtmlKanji(data));
                intent.putExtra("newWord", data[0]);
                startActivity(intent);
            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        cursor.close();
        dataBaseHeplper.close();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_kanji, menu);
        MenuItem searchItem = menu.findItem(R.id.action_search);
        searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
        searchView.setQueryHint("Từ khóa");
        setupSearchView();
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                break;
            case R.id.kanji_search:
                queryCode = 0;
                break;
            case R.id.hira_search:
                queryCode = 1;
                break;
            case R.id.kata_search:
                queryCode = 2;
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);

    }
    private void setupSearchView() {
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }
            @Override
            public boolean onQueryTextChange(String newText) {
                String keyWord;
                String nameColumn;
                if (queryCode == 1) {
                    keyWord = wanaKana.toHiragana(newText);
                    nameColumn = "kunread_meaning";
                } else if (queryCode == 2) {
                    keyWord = wanaKana.toKatakana(newText);
                    nameColumn = "onread";
                } else {
                    keyWord = newText;
                    nameColumn = "hanviet";
                }
                Cursor newCursor = dataBaseHeplper.getInformation(TABLE_NAME, nameColumn, keyWord);
                if (newCursor.getCount() == 0);/*
                    Toast.makeText(getBaseContext(), "Wrong key", Toast.LENGTH_LONG).show();*/
                else {
                    cursor = newCursor;
                    adapter.changeCursor(cursor);
                }
                return false;
            }
        });
    }
    @Override
    public void onBackPressed(){
        if(canBack)
            super.onBackPressed();
        else
            Toast.makeText(this, "Đang lấy dữ liệu, vui lòng chờ!", Toast.LENGTH_SHORT).show();
    }
}