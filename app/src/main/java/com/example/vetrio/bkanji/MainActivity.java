package com.example.vetrio.bkanji;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;

import com.example.vetrio.adapter.MainListViewAdapter;

public class MainActivity extends AppCompatActivity {
    public ListView main_lw;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Gan dia chi id cho cac doi tuong da tao voi giao dien tren xml
        main_lw = (ListView) findViewById(R.id.main_lw);

        //Cai dat menu list
        MainListViewAdapter mainListViewAdapter = new MainListViewAdapter(this);
        main_lw.setAdapter(mainListViewAdapter);
        main_lw.setOnItemClickListener(null);
    }

    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

}
