package com.example.vetrio.bkanji;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.SQLException;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.vetrio.database.DataBaseHeplper;

public class JlptTestActivity extends AppCompatActivity {

    private TextView quersion;
    private Button[] answ;
    private TextView loai, nmay, point, time;
    private int pointInt = 0;
    private int cauhoi = 1;

    private String stype[] = {"漢字", "単語", "文法"};

    private static final String DB_NAME = "JLPTTest.db";
    private static final String TABLE_NAME1 = "Question";
    private static final String COLUMN_NAME1 = "TypeId";
    private static final String TABLE_NAME2 = "Answer";
    private static final String COLUMN_NAME2 = "Q_id";
    public DataBaseHeplper dataBaseHeplper;
    public Cursor cursor1;
    public Cursor cursor2;


    private boolean canBack = false;
    private int code = 0;
    private int n = 0;
    private Context ctx = this;
    private int id = 0;
    private String answer = "";
    private CountDownTimer timer1, timer2, timer3;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_jlpt_test);

        Intent intent = getIntent();
        n = intent.getIntExtra("n", 0);
        code = intent.getIntExtra("code", 1);
        id = n * 3 + code;

        loai = (TextView) findViewById(R.id.loai);
        nmay = (TextView) findViewById(R.id.n);
        point = (TextView) findViewById(R.id.point);
        time = (TextView) findViewById(R.id.timer);

        quersion = (TextView) findViewById(R.id.question);

        answ = new Button[4];
        answ[0] = (Button) findViewById(R.id.ans1);
        answ[1] = (Button) findViewById(R.id.ans2);
        answ[2] = (Button) findViewById(R.id.ans3);
        answ[3] = (Button) findViewById(R.id.ans4);

        loai.setText(stype[code-1]);
        nmay.setText("N"+ (n + 1));

        for (int i = 0; i < answ.length; i++){
            final int finalI = i;
            answ[i].setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    answ[finalI].setBackgroundResource(R.drawable.bg_btn_answer_orange);

                    timer2 = new CountDownTimer(750, 250) {
                        @Override
                        public void onTick(long l) {
                        }
                        @Override
                        public void onFinish() {
                            timer1.cancel();
                            timer2.cancel();
                            newQuestion(cauhoi, pointInt);
                        }
                    };
                    timer2.start();

                    timer1 = new CountDownTimer(500, 250) {
                        @Override
                        public void onTick(long l) {
                        }

                        @Override
                        public void onFinish() {
                            if (answ[finalI].getText().equals(answer)) {
                                answ[finalI].setBackgroundResource(R.drawable.bg_btn_answer_green);
                                pointInt++;
                            } else {
                                answ[finalI].setBackgroundResource(R.drawable.bg_btn_answer_red);
                                for (int j = 0; j < 4; j++) {
                                    if (answ[j].getText().equals(answer))
                                        answ[j].setBackgroundResource(R.drawable.bg_btn_answer_green);
                                }
                            }
                            cauhoi++;
                            timer2.start();
                        }
                    };
                    timer1.start();
                }
            });
        }

        timer3 = new CountDownTimer(20000, 1000) {
            @Override
            public void onTick(long l) {
                time.setText((l/1000) +"");
            }

            @Override
            public void onFinish() {
                for (int j = 0; j < 4; j++)
                    if (answ[j].getText().equals(answer))
                        answ[j].setBackgroundResource(R.drawable.bg_btn_answer_green);
                doimotchut();
            }
        };
        timer3.start();

        //CreateDatabase
        dataBaseHeplper = new DataBaseHeplper(this, DB_NAME);
        try {
            dataBaseHeplper.openDataBase();
        } catch (SQLException sqle) {
            throw sqle;
        }

        AsyncTask<Void, Integer, Void> asyncTask = new AsyncTask<Void, Integer, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                canBack = false;
                cursor1 = dataBaseHeplper.getInformation(TABLE_NAME1, COLUMN_NAME1, id);
                cursor1.moveToFirst();
                cursor2 = dataBaseHeplper.getInformation(TABLE_NAME2, COLUMN_NAME2, cursor1.getInt(0));
                cursor2.moveToFirst();
                return null;
            }
            protected void onPostExecute(Void result) {
                canBack = true;
                quersion.setText("問題１：\n"+cursor1.getString(2));
                for (int i = 0; i < answ.length; i++){
                    answ[i].setText(cursor2.getString(i + 2));
                }
                answer = cursor2.getString(6);
            }
        };
        asyncTask.execute();

    }

    public void newQuestion(int q, int p){
        timer3.cancel();
        if(cursor1.isLast())  showMyDialog(p);
        cursor1.moveToNext();
        quersion.setText("問題" + q + ":\n\n" + cursor1.getString(2));
        cursor2 = dataBaseHeplper.getInformation(TABLE_NAME2, COLUMN_NAME2, cursor1.getInt(0));
        cursor2.moveToFirst();
        for (int i = 0; i < answ.length; i++){
            answ[i].setBackgroundResource(R.drawable.bg_btn_answer);
            answ[i].setText(cursor2.getString(i + 2));
        }
        answer = cursor2.getString(6);
        point.setText(p+"/250");
        timer3.start();
    }

    public void doimotchut(){
        timer2 = new CountDownTimer(750, 250) {
            @Override
            public void onTick(long l) {
            }
            @Override
            public void onFinish() {
                timer2.cancel();
                newQuestion(cauhoi++, pointInt);
            }
        };
        timer2.start();
    }

    public void showMyDialog(int point){
        final AlertDialog alertDialog = new AlertDialog.Builder(this).create();

        // Setting Dialog Title
        alertDialog.setTitle("終わります");

        // Setting Dialog Message
        alertDialog.setMessage("Bạn đã đúng được " + point + "/250 câu hỏi");

        // Setting OK Button
        alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                alertDialog.dismiss();
                finish();
            }
        });
        alertDialog.show();
    }
    @Override
    public void onDestroy() {
        super.onDestroy();
        if(timer1 != null)
            timer1.cancel();
        if(timer2 != null)
            timer2.cancel();
        if(timer3 != null)
            timer3.cancel();
        if(cursor1 != null)
            cursor1.close();
        if(cursor2 != null)
            cursor2.close();
        if(dataBaseHeplper != null)
            dataBaseHeplper.close();
    }

    @Override
    public void onBackPressed(){
        if(canBack)
            super.onBackPressed();
        else
            Toast.makeText(this, "Đang lấy dữ liệu, vui lòng chờ!", Toast.LENGTH_SHORT).show();
    }
}