package com.example.vetrio.bkanji;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.vetrio.database.GameData;
import com.example.vetrio.database.GameItem;

public class GamePlayActivity extends AppCompatActivity {
    private Button btn1, btn2;
    private ProgressBar progressBar;
    private TextView question;
    private TextView score;
    int point = 0;
    private boolean islost = false;
    private GameData gameData;
    private GameItem[]items;
    MyCountDownTimer timer;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game_play);

        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        progressBar.setMax(100);
        progressBar.setProgress(0);
        progressBar.getProgressDrawable().setColorFilter(Color.BLUE, PorterDuff.Mode.SRC_IN);

        btn1 = (Button) findViewById(R.id.btn1);
        btn2 = (Button) findViewById(R.id.btn2);
        question = (TextView) findViewById(R.id.question_tv);
        score = (TextView) findViewById(R.id.score);
        getQuestion();

        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                timer.cancel();
                if (gameData.answer == 1) {
                    point++;
                    getQuestion();
                    if(islost == true) endGame();
                }else{
                    endGame();
                }

            }
        });

        btn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                timer.cancel();
                if(gameData.answer == 2) {
                    point ++;
                    getQuestion();
                    if(islost == true) endGame();
                }else{
                    endGame();
                }

            }
        });

    }

    public class MyCountDownTimer extends CountDownTimer {

        public MyCountDownTimer(long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval);
            progressBar.setProgress(0);
        }

        @Override
        public void onTick(long millisUntilFinished) {
            int progress = (int) (millisUntilFinished/15);
            progressBar.setProgress(progress);
        }

        @Override
        public void onFinish() {
            progressBar.setProgress(0);
            btn1.setEnabled(false);
            btn2.setEnabled(false);
            endGame();
        }

    }

    public void getQuestion(){
        score.setText("Điểm: " + point);
        gameData = new GameData();
        items = new GameItem[3];
        items = gameData.gameItems(gameData.mapGame());

        question.setText(gameData.mauTV[items[0].getTextPosition()] + "\n" +
                gameData.mauTN[items[0].getTextPosition()] + "(" + gameData.mauKJ[items[0].getTextPosition()] + ")");
        question.setTextColor(getResources().getColor(gameData.color[items[0].getColorPostion()]));

        btn1.setText(gameData.mauTV[items[1].getTextPosition()]);
        btn1.setTextColor(getResources().getColor(gameData.color[items[1].getColorPostion()]));

        btn2.setText(gameData.mauTV[items[2].getTextPosition()]);
        btn2.setTextColor(getResources().getColor(gameData.color[items[2].getColorPostion()]));
        timer = new MyCountDownTimer(1500, 15);
        timer.start();
    }


    public void endGame(){
        CustomDialogClass dialogClass = new CustomDialogClass(this);
        dialogClass.show();
    }


    public class CustomDialogClass extends Dialog implements
            android.view.View.OnClickListener {
        public Activity c;
        TextView score;
        Button choilaiBtn;
        Button quitBtn;
        public CustomDialogClass(Activity a) {
            super(a);
            // TODO Auto-generated constructor stub
            this.c = a;
        }

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            requestWindowFeature(Window.FEATURE_NO_TITLE);
            setContentView(R.layout.dialog_end_game);

            score = (TextView) findViewById(R.id.score);
            score.setText("Điểm: " + point);
            choilaiBtn = (Button) findViewById(R.id.choilaiBtn);
            quitBtn = (Button) findViewById(R.id.quitBtn);

            choilaiBtn.setOnClickListener(this);
            quitBtn.setOnClickListener(this);

        }

        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.choilaiBtn:
                    point = 0;
                    dismiss();
                    btn1.setEnabled(true);
                    btn2.setEnabled(true);
                    getQuestion();

                    break;
                case R.id.quitBtn:
                    dismiss();
                    c.finish();
                    break;
                default:
                    break;
            }
            dismiss();
        }
    }
}

