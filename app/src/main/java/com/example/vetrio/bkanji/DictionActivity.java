package com.example.vetrio.bkanji;

import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.vetrio.adapter.DictionPageViewAdapter;
import com.example.vetrio.fragment.DictFragment;


public class DictionActivity extends AppCompatActivity {

    private ViewPager viewPager;
    private Toolbar toolbar;
    private SearchView searchView;
    private View[] tab_indicator;
    private static final int COUNT_TAB = 2;
    private int[] tabIndicatorID = {R.id.tab1_indicator, R.id.tab2_indicator};
    private TextView[] tab;
    private int[] tabID = {R.id.tab1, R.id.tab2};
    private DictionPageViewAdapter dictionPageViewAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_diction);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        viewPager = (ViewPager) findViewById(R.id.pager);
        //Connect tab
        tab_indicator = new View[COUNT_TAB];
        tab = new TextView[COUNT_TAB];

        for (int i = 0; i < COUNT_TAB; i++) {
            tab_indicator[i] = findViewById(tabIndicatorID[i]);
            tab[i] = (TextView) findViewById(tabID[i]);
            final int finalI = i;
            tab[i].setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    viewPager.setCurrentItem(finalI);
                }
            });
        }

        toolbar.setTitle("Tra từ mới");
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        dictionPageViewAdapter = new DictionPageViewAdapter(getSupportFragmentManager());
        viewPager.setAdapter(dictionPageViewAdapter);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                onIndicatorTabSelected(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_diction, menu);
        MenuItem searchItem = menu.findItem(R.id.action_search);
        searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
        searchView.setQueryHint("Từ khóa");
        setupSearchView();
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setupSearchView() {
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                dictionPageViewAdapter.notifyNewQuery(newText);
                return false;
            }
        });
    }


    public void onIndicatorTabSelected(int position) {
        for (int i = 0; i < COUNT_TAB; i++) {
            if (i == position) tab_indicator[i].setBackgroundResource(android.R.color.white);
            else tab_indicator[i].setBackgroundResource(R.color.transparent);
        }

    }
    @Override
    public void onBackPressed(){
        if(DictFragment.canBack)
            super.onBackPressed();
        else
            Toast.makeText(this, "Đang lấy dữ liệu, vui lòng chờ!", Toast.LENGTH_SHORT).show();
    }
}
