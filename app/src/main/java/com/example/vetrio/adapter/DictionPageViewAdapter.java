package com.example.vetrio.adapter;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.example.vetrio.fragment.DictFragment;

/**
 * Created by nguye on 9/21/2015.
 */
public class DictionPageViewAdapter extends FragmentStatePagerAdapter {

    private DictFragment mJaviFragment;
    private DictFragment mVijaFragment;

    public DictionPageViewAdapter(FragmentManager fm) {
        super(fm);
        // TODO Auto-generated constructor stub
    }

    @Override
    public Fragment getItem(int position) {
        Bundle bundle = new Bundle();
        switch (position) {
            case 0:
                mJaviFragment = new DictFragment();
                bundle.putInt("position", position);
                mJaviFragment.setArguments(bundle);
                return mJaviFragment;
            case 1:
                mVijaFragment = new DictFragment();
                bundle.putInt("position", position);
                mVijaFragment.setArguments(bundle);
                return mVijaFragment;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return 2;
    }

    public void notifyNewQuery(String query) {
        if (query == null || query.equals("")) {
            return;
        }

        if (mJaviFragment != null) {
            mJaviFragment.setNewQueryText(query);
        }
        if (mVijaFragment != null) {
            mVijaFragment.setNewQueryText(query);
        }
    }
}
