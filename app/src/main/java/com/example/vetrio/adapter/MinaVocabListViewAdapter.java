package com.example.vetrio.adapter;

import android.content.Context;
import android.database.Cursor;
import android.support.v4.widget.CursorAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.vetrio.bkanji.R;

public class MinaVocabListViewAdapter extends CursorAdapter {

    private int[] bg = {
            R.drawable.c1,
            R.drawable.c2,};

    public MinaVocabListViewAdapter(Context context, Cursor cursor) {
        super(context, cursor, true);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return LayoutInflater.from(context).inflate(R.layout.items_listview_mina_voca, parent, false);
    }

    @Override
    public void bindView(View convertView, Context context, Cursor cursor) {
        // Find fields to populate in inflated template
        TextView tiengnhat = (TextView) convertView.findViewById(R.id.tiengnhat);
        TextView tienghan = (TextView) convertView.findViewById(R.id.tienghan);
        TextView tiengviet = (TextView) convertView.findViewById(R.id.tiengviet);
        tiengnhat.setText(cursor.getString(1));
        tienghan.setText(cursor.getString(2));
        tiengviet.setText(cursor.getString(3));
    }
}