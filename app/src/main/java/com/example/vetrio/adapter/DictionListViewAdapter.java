package com.example.vetrio.adapter;

import android.content.Context;
import android.database.Cursor;
import android.support.v4.widget.CursorAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.vetrio.bkanji.R;
import com.example.vetrio.converter.ConvertString;

public class DictionListViewAdapter extends CursorAdapter {
    public DictionListViewAdapter(Context context, Cursor cursor) {
        super(context, cursor, true);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return LayoutInflater.from(context).inflate(R.layout.items_listview_diction, parent, false);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        // Find fields to populate in inflated template
        TextView word_tv = (TextView) view.findViewById(R.id.word_tv);
        TextView content_tv = (TextView) view.findViewById(R.id.content_tv);

        word_tv.setText(cursor.getString(1));
        content_tv.setText("Nghĩa: \t" + new ConvertString().getMean(cursor.getString(2)));
    }
}