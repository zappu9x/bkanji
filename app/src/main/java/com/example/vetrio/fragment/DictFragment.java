package com.example.vetrio.fragment;

import android.content.Intent;
import android.database.Cursor;
import android.database.SQLException;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.example.vetrio.adapter.DictionListViewAdapter;
import com.example.vetrio.bkanji.R;
import com.example.vetrio.bkanji.ResultSearchActivity;
import com.example.vetrio.converter.ConvertString;
import com.example.vetrio.converter.KanaConverter;
import com.example.vetrio.database.DataBaseHeplper;

/**
 * Created by nguye on 9/21/2015.
 */
public class DictFragment extends Fragment {


    private static final String[] DB_NAME = {"japanese_vietnamese.db", "vietnamese_japanese.db"};
    private static final String[] TABLENAME = {"japanese_vietnamese", "vietnamese_japanese"};
    private static final String COLUMN_NAME = "word";

    public static boolean canBack = false;

    private String textSubmit = "";
    private String hiraganaText = "";
    private Cursor cursor;
    private DataBaseHeplper dataBaseHeplper;

    public ListView listView;
    private ProgressBar progressBar;
    private int position;
    private DictionListViewAdapter adapter;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle bundle = getArguments();
        position = bundle.getInt("position");

        dataBaseHeplper = new DataBaseHeplper(getActivity().getBaseContext(), DB_NAME[position]);
        try {
            dataBaseHeplper.openDataBase();
        } catch (SQLException sqle) {
            throw sqle;
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_diction, container, false);
        listView = (ListView) view.findViewById(R.id.list);
        progressBar = (ProgressBar) view.findViewById(R.id.progressBar);
        queryDatabase();
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent(getActivity().getBaseContext(), ResultSearchActivity.class);
                intent.putExtra("dataHtml", new ConvertString().getHtml(cursor.getString(1), cursor.getString(2)));
                intent.putExtra("newWord", cursor.getString(1));
                startActivity(intent);
            }
        });
        return view;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        cursor.close();
        dataBaseHeplper.close();
    }

    public void setNewQueryText(String queryText) {
        textSubmit = queryText;
        queryDatabase();
    }

    private void queryDatabase() {
        progressBar.setVisibility(View.VISIBLE);
        AsyncTask<Void, Integer, Void> asyncTask = new AsyncTask<Void, Integer, Void>() {
            Cursor newCursor;
            @Override
            protected Void doInBackground(Void... voids) {
                canBack = false;
                try {
                    hiraganaText = KanaConverter.toHiragana(textSubmit);
                } catch (Exception e) {
                    /*Toast.makeText(getActivity().getBaseContext(), "Sai từ khóa", Toast.LENGTH_SHORT).show();*/
                }
                newCursor = dataBaseHeplper.getInformation(TABLENAME[position], COLUMN_NAME, textSubmit, hiraganaText);
                newCursor.moveToFirst();
                return null;
            }

            protected void onPostExecute(Void result) {
                canBack = true;
                if (newCursor.getCount() == 0) {
                    /*Toast.makeText(getActivity().getBaseContext(), "Sai từ khóa", Toast.LENGTH_SHORT).show();*/
                }
                else {
                    cursor = newCursor;
                    adapter = new DictionListViewAdapter(getActivity().getBaseContext(), cursor);
                    listView.setAdapter(adapter);
                }
                progressBar.setVisibility(View.GONE);
            }
        };
        asyncTask.execute();
    }
}