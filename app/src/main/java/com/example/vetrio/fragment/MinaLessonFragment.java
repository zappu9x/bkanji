package com.example.vetrio.fragment;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;

import com.example.vetrio.bkanji.R;

public class MinaLessonFragment extends Fragment {

    private String data;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    public void setData(String data){
        this.data = data;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_mina_tab1, container, false);
        WebView webView = (WebView) view.findViewById(R.id.wv);
        webView.setBackgroundColor(Color.TRANSPARENT);

        WebSettings webSettings = webView.getSettings();
        webSettings.setDefaultFontSize(18);

        webView.loadDataWithBaseURL(null, data, "text/html", "utf-8",null);
        return view;
    }
}