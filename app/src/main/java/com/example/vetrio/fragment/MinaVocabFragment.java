package com.example.vetrio.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.example.vetrio.adapter.MinaVocabListViewAdapter;
import com.example.vetrio.bkanji.R;

public class MinaVocabFragment extends Fragment {

    private MinaVocabListViewAdapter adapter;

    @Override
    public void onCreate(Bundle savedInstanceState ) {
        super.onCreate(savedInstanceState);

    }

    public void setAdapter(MinaVocabListViewAdapter adapter){
        this.adapter = adapter;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_mina_tab3, container, false);
        ListView listView = (ListView) view.findViewById(R.id.list);
        listView.setAdapter(adapter);
        return view;
    }
}