package com.example.vetrio.database;

import com.example.vetrio.bkanji.R;

/**
 * Created by VetRio on 8/29/2015.
 */
public class GameData{

    private int count;
    public int color[];
    public String mauTV[];
    public String mauTN[];
    public String mauKJ[];
    public int answer = 1;

    public GameData() {
        count = 11;
        color = new int[]{
                R.color.xanhdatroi,
                R.color.maudo,
                R.color.trang,
                R.color.den,
                R.color.cam,
                R.color.hong,
                R.color.tim,
                R.color.vang,
                R.color.nau,
                R.color.xanhlacay,
                R.color.xam
        };
        mauKJ = new String[]{"青", "赤", "白", "黒", "橙色", "桃色", "紫", "黄色", "茶色", "緑", "灰色"};
        mauTV = new String[]{ "Xanh da trời", "Đỏ", "Trắng", "Đen", "Cam", "Hồng", "Tím", "Vàng", "Nâu", "Xanh lá cây", "Xám"};
        mauTN = new String[]{ "あお", "あか", "しろ", "くろ", "だいだいいろ", "ももいろ", "むらさき", "きいろ", "ちゃいろ", "みどり", "はいいろ"};
    }
    public int getCount(){
        return count;
    }

    public int[] mapGame(){
        int mapInts[] = new int[8];
        mapInts[0] = (int)(Math.random()*(11));//Mau chu cau hoi
        mapInts[1] = 1 + (int)(Math.random()*(2)); // Cau tra loi dung
        mapInts[2] = (int)(Math.random()*(11)); //Ten chu cau hoi
        mapInts[3] = (int)(Math.random()*(3)); // Cau tra loi sai lua
        mapInts[4] = (int)(Math.random()*(11)); //Mau chu dap an sai
        mapInts[5] = (int)(Math.random()*(2)); //Cau tra loi dung lua
        mapInts[6] = (int)(Math.random()*(10));
        if (mapInts[6] >= mapInts[0]) mapInts[6]++;

        mapInts[7] = (int)(Math.random()*(10));
        if (mapInts[7] >= mapInts[0]) mapInts[7]++;

        return mapInts;
    }

    public GameItem[] gameItems(int map[]){
        GameItem []items = new GameItem[3];
        for (int i = 0; i < 3; i++){
            items[i] = new GameItem();
        }
        items[0].setColorPostion(map[0]);
        items[0].setTextPosition(map[2]);

        //Ten chu cho Dap an dung
        items[map[1]].setTextPosition(map[0]);//gia tri dung la map[1]

        int dapansai = 1;
        int dapandung = map[1];
        if(map[1] == 1) dapansai = 2;
        answer = dapandung;

        /*
        Mau chu cho dap an sai
        **/
        if(map[3] == 0)
            items[dapansai].setColorPostion(items[0].getTextPosition());
        if (map[3] == 1)
            items[dapansai].setColorPostion(items[0].getColorPostion());
        else items[dapansai].setColorPostion(map[4]);

        /*
        Mau chu cho dap an dung
        **/
        if(map[5] == 0) //Khong lua
            items[dapandung].setColorPostion(items[dapandung].getTextPosition());
        else items[dapandung].setColorPostion(map[6]);

        /*
        Ten chu cho dap an dung
        **/
        items[dapansai].setTextPosition(map[7]);

        return items;
    }



}