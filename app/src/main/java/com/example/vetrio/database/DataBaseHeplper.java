package com.example.vetrio.database;

import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class DataBaseHeplper extends SQLiteOpenHelper {
    private static final String dbPath = "/data/data/com.example.vetrio.bkanji/databases/";

    private String nameDB;
    private int totalData;
    private final Context context;
    private int byteCopyed = 0;

    //Constructor
    public DataBaseHeplper(Context context, String nameDB, int totalData) {
        super(context, nameDB, null, 1);
        this.context = context;
        this.nameDB = nameDB;
        this.totalData = totalData;
    }

    public DataBaseHeplper(Context context, String nameDB) {
        super(context, nameDB, null, 1);
        this.context = context;
        this.nameDB = nameDB;
    }

    public void createDataBase() throws IOException {
        boolean dbExist = checkDataBase();
        if (dbExist) {
            //do nothing - database already exist
        } else {
            this.getReadableDatabase();
            try {
                copyDataBase();
            } catch (IOException e) {
                throw new Error("Error copying database");
            }
        }
    }

    public boolean checkDataBase() {

        SQLiteDatabase checkDB = null;
        try {
            checkDB = openDataBase();
        } catch (SQLiteException e) {
            //database does't exist yet.
        }
        if (checkDB != null) {
            checkDB.close();
        }
        return checkDB != null ? true : false;
    }

    private void copyDataBase() throws IOException {

        final InputStream myInput = context.getAssets().open(nameDB);
        String outFileName = dbPath + nameDB;
        final OutputStream myOutput = new FileOutputStream(outFileName);

        final byte[] buffer = new byte[1024];


        File filenew = new File(nameDB);

        int length;
        int copyedData = 0;

        //Copy data
        try {
            while ((length = myInput.read(buffer)) > 0) {
                myOutput.write(buffer, 0, length);
                ++copyedData;
                setByteCopyed(copyedData);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        //Close database
        try {
            myOutput.flush();
            myOutput.close();
            myInput.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public SQLiteDatabase openDataBase() throws SQLException {
        String myPath = dbPath + nameDB;
        SQLiteDatabase sqLiteDatabase = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.NO_LOCALIZED_COLLATORS);
        sqLiteDatabase.close();
        return sqLiteDatabase;
    }

    public Cursor getInformation(String nameTable, String columnName, String wordQuery) {
        SQLiteDatabase SQL = this.getReadableDatabase();
        Cursor result;
        try {
            result = SQL.rawQuery("Select * from " + nameTable + " where " + columnName + " like \"" + wordQuery + "%\"", null);
        } catch (Exception e) {
            result = null;
        }
        return result;
    }

    public Cursor getInformation(String nameTable, String columnName, int numberQuery) {
        SQLiteDatabase SQL = this.getReadableDatabase();
        Cursor result;
        try {
            result = SQL.rawQuery("Select * from " + nameTable + " where " + columnName + "==" + numberQuery, null);
        } catch (Exception e) {
            result = null;
        }
        return result;
    }
    public Cursor getInformation(String nameTable, String columnName, String wordQuery, String wordQuery2) {
        SQLiteDatabase SQL = this.getReadableDatabase();
        Log.d("123456", wordQuery2);
        Cursor result;
        try {
            result = SQL.rawQuery("Select * from " + nameTable + " where " + columnName + " like \"" + wordQuery + "%\"" +
                    " or " + columnName + " like \"" + wordQuery2 + "%\"" , null);
        } catch (Exception e) {
            result = null;
        }
        return result;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // TODO Auto-generated method stub
    }

    public int getByteCopyed() {
        return byteCopyed;
    }

    public void setByteCopyed(int byteCopyed) {
        this.byteCopyed = byteCopyed;
    }
}
