package com.example.vetrio.database;

/**
 * Created by nguye on 11/10/2015.
 */
public class GameItem {

    private int colorPostion;
    private int textPosition;

    public GameItem(){}

    public int getColorPostion() {
        return colorPostion;
    }

    public void setColorPostion(int colorPostion) {
        this.colorPostion = colorPostion;
    }



    public int getTextPosition() {
        return textPosition;
    }

    public void setTextPosition(int textPosition) {
        this.textPosition = textPosition;
    }
}
